const intialState = {
    todos:[{data:'First....'},
           {data:'Second....'}]
}

const rootReducer = (state = intialState,action) => {

    switch(action.type){
        case 'ADD_TODO':
          return { ...state,todos:[...state.todos,action.payload]};
        case 'REMOVE_TODO':
          return { ...state,todos:state.todos.filter((item,index) => {return index !== action.payload})};
        case 'SIGNOUT':
           return intialState;
        default:
          return state;
    }

};

export default rootReducer;