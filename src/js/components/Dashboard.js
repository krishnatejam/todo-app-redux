import React,{ Component } from 'react';
import '../../css/Dashboard.css';
import { connect } from 'react-redux';
import { addTodo,removeTodo,signOut } from '../actions/index';

const mapStateToProps = (state) => {
    return {
        todos : state.todos
    };
}

const mapDispatchToProps = (dispatch) => {
     return {
        addTodo: (todo) => dispatch(addTodo(todo)),
        removeTodo : (i) => dispatch(removeTodo(i)),
        signOut : () => dispatch(signOut())
     }
}


class DashbaordComponent extends Component{

    updateTodos = () => {
        if(this.inputTodo.value){
            this.props.addTodo({data:this.inputTodo.value});
        }
        this.inputTodo.value = null;
    }

    remove = (i) => {
         this.props.removeTodo(i);
    }

    logout = () => {
       this.props.signOut();
       this.props.history.push('/');
    }

    render(){
        var todoLists = this.props.todos.map(function(item,i){;
            return(
               <div className="style-todo-block" key={i}>
                  <div className="style-delete-icon" onClick={() => this.remove(i)}>
                     <i className="fa fa-trash" aria-hidden="true"></i>
                  </div>
                  <div className="style-todo">
                    {item.data}
                  </div>
               </div>
            );
        },this)

        return(
              <div>
                  <div className="todo-adder">
                     <input className="todo-input" type="text" placeholder="Enter Here..." ref={el => this.inputTodo = el} onChange={this.updateInput}/>
                     <button className="todo-add-button" onClick={this.updateTodos}>Add</button>
                     <button className="Log-out-button" onClick={this.logout}>Logout</button>
                  </div>
                  <div>{todoLists}</div>
              </div>
        );
    }

}

const Dashbaord = connect(mapStateToProps,mapDispatchToProps)(DashbaordComponent);

export default Dashbaord;