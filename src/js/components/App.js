import React, { Component } from 'react';
import {Route,Switch} from 'react-router-dom';
import Login from './Login';
import Dashbaord from './Dashboard';
import '../../../node_modules/font-awesome/css/font-awesome.min.css'; 


class App extends Component {
    render(){
       return(
        <div>
            <Switch>
                <Route exact path='/' component={Login} />
                <Route exact path='/login' component={Login} />
                <Route exact path='/dashboard' component={Dashbaord} />
            </Switch>
         </div>
       );
    }
    
}

export default App;