import React, { Component } from 'react';
import '../../css//Login.css';

class Login extends Component {

  constructor(props){
      super(props);
      this.state = {
        username:null,
        password:null,
        incorrectPass : false
      }
  }

  checkLogin = () => {
      if(this.userName.value && this.password.value && this.userName.value.toLowerCase().trim() === 'krishna' && this.password.value.toLowerCase().trim() === 'krishna'){
        this.setState({
          incorrectPass:false
        })
        this.props.history.push('/dashboard');
      } else {
        this.setState({
          incorrectPass : true
        })
      }
  }


  render() {
    return (
       <div className = "login-container">
           <div className="header">Login</div>
           <div>
             <input className="input-box" type="text" name="username" placeholder="userName" ref={el => this.userName = el} />
             <input className="input-box" type="password" name="password" placeholder="password" ref={el => this.password = el} />
           </div>
            <div>
              <button className="login-button" onClick = {this.checkLogin}>Login!</button>
              {this.state.incorrectPass && <div className="incorrect-password-text"> incorrect username or password !!!</div>}
            </div>
       </div>
    );
  }
}

export default Login;
