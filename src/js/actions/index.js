export const addTodo = (todo) => ({ type:"ADD_TODO",payload:todo});

export const removeTodo = (i) =>({type:"REMOVE_TODO",payload:i});

export const signOut = () => ({type:"SIGNOUT",payload:null });